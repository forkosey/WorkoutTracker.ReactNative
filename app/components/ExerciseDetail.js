import React, { Component } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  Switch,
  View,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import Button from "react-native-button";
import { AutoGrowingTextInput } from "react-native-autogrow-textinput";
import { Icon } from "react-native-elements";

import SetFlatList from "./SetFlatList";
import {
  ExerciseDetailScreen,
  NewExerciseScreen,
  SaveExercise,
  SaveNewExercise,
  NewWoSetScreen,
  SaveNewWoSet,
  SaveWoSet
} from "../../screenNames";

var uuid = require('react-native-uuid');

var exercise = null;

export default class ExerciseDetail extends Component {
  constructor(props) {
    super(props);
    var key = null;
    var name = "Exercise Name";
    var desc = "Description";
    var setList = [];
    if (exercise != null) {
      key = exercise.key;
      name = exercise.name;
      desc = exercise.desc;
      setList = exercise.setList;
    }
    this.state = {
      exercise: exercise,
      key: key,
      exerciseName: name,
      exerciseDesc: desc,
      setList: setList
    };
  }

  getChildSet = (woSet, saveType) => {
    this.setState({
      childScreenSet: woSet,
      saveType: saveType
    });
  };

  setSaving = (woSet, saveType) => {
    if (saveType == SaveNewWoSet) {
      var newSetList = this.state.setList;
      newSetList.push(woSet);
      this.setState({setList: newSetList});
    } else if (saveType == SaveWoSet) {
      var newSetList = this.state.setList;
      var index = 0;
      for (var i = 0; i < newSetList.length; i++) {
        if (newSetList[i].key == woSet.key) {
          index = i;
          break;
        }
      }
      newSetList[index] = woSet;
      this.setState({setList: newSetList});
    }
  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    var headerTitle = "New Exercise";
    if (navigation.state.routeName == NewExerciseScreen) {
      exercise = null;
    } else if (navigation.state.routeName == ExerciseDetailScreen) {
      exercise = navigation.getParam("exercise", null);
      headerTitle = exercise.name;
    }

    let headerRight = (
      <Button
        containerStyle={{
          margin: 5,
          padding: 10,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: "#4997f1"
        }}
        style={{ fontSize: 12, color: "#4997f1" }}
        onPress={() => {
          params.onSave();
        }}
      >
        Save
      </Button>
    );

    let headerBackTitle = "Cancel";

    return { headerTitle, headerRight, headerBackTitle };
  };

  _onSave() {
    if (this.props.navigation.state.params.isSaving == true) {
      return;
    }
    this.props.navigation.setParams({ isSaving: true });
    if (this.state.exercise != null) {
      this.props.navigation.setParams({ isSaving: false });
      this.props.navigation.state.params.exerciseSaving(
        {
          key: this.state.key,
          name: this.state.exerciseName,
          desc: this.state.exerciseDesc,
          setList: this.state.setList
        },
        SaveExercise
      );
      this.props.navigation.setParams({ isSaving: false });
    } else {
      this.props.navigation.state.params.exerciseSaving(
        {
          key: uuid.v4(),
          name: this.state.exerciseName,
          desc: this.state.exerciseDesc,
          setList: this.state.setList
        },
        SaveNewExercise
      );
      this.props.navigation.setParams({ isSaving: false });
    }
    this.props.navigation.goBack();
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSave: this._onSave.bind(this),
      isSaving: false
    });
  }

  render() {
    const { navigation } = this.props;

    _newSetButtonOnPress = () => {
      navigation.navigate(NewWoSetScreen, {
        getChildSet: this.getChildSet.bind(this),
        setSaving: this.setSaving.bind(this)
      });
    };

    let mainView =
      this.props.navigation.state.params &&
      this.props.navigation.state.params.isSaving == true ? (
        <ActivityIndicator style={{ flex: 1 }} />
      ) : (
        <View style={styles.mainView}>
          <TextInput
            style={styles.exerciseName}
            onSubmitEditing={Keyboard.dismiss}
            underlineColorAndroid="transparent"
            placeholder={this.state.exerciseName}
            onChangeText={text => {
              this.setState({ exerciseName: text });
            }}
            value={this.state.exerciseName}
          />
          <TextInput
            style={styles.exerciseDesc}
            multiline={true}
            underlineColorAndroid="transparent"
            placeholder={this.state.exerciseDesc}
            onChangeText={text => {
              this.setState({ exerciseDesc: text });
            }}
            value={this.state.exerciseDesc}
          />
          <View style={styles.setListHeader}>
            <Icon name="add" color="rgba(0,0,0,0)" />
            <Text style={{ fontWeight: "bold", fontSize: 16 }}>Sets</Text>
            <Icon name="add" color="#4997f1" onPress={_newSetButtonOnPress}/>
          </View>
          <SetFlatList
            sets={this.state.setList}
            parentDetailView={this}
          />
        </View>
      );

    return mainView;
  }
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#f7faff"
  },
  exerciseName: {
    marginHorizontal: 10,
    marginTop: 8,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    fontWeight: "bold"
  },
  exerciseDesc: {
    height: 150,
    marginHorizontal: 10,
    marginTop: 8,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    textAlignVertical: "top"
  },
  setListHeader: {
    backgroundColor: "#e3e9f2",
    marginTop: 8,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 8
  }
});
