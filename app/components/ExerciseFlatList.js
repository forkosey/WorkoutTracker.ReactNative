import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";

import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";

import { NewWorkoutScreen } from "../../screenNames";

import ExerciseListCell from "./ExerciseListCell";

export default class ExerciseFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deletedRowKey: null
    };
  }

  refreshFlatList = deletedKey => {
    this.setState(prevState => {
      return {
        deletedRowKey: deletedKey
      };
    });
  };

  render() {
    const { navigation } = this.props.parentDetailView.props;

    return (
      <View style={{ flex: 1, backgroundColor: "#f2f7ff" }}>
        <FlatList
          data={this.props.exercises}
          extraData={this.props.parentDetailView.state}
          renderItem={({ item, index }) => {
            return (
              <ExerciseListCell
                item={item}
                index={index}
                parentFlatList={this}
              />
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});
