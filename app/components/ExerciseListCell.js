import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View,
  Alert,
  TouchableWithoutFeedback
} from "react-native";

import { Icon } from "react-native-elements";
import Swipeout from "react-native-swipeout";

import { ExerciseDetailScreen } from "../../screenNames";

export default class ExerciseListCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeRowKey: null
    };
  }

  render() {
    const {
      navigation
    } = this.props.parentFlatList.props.parentDetailView.props;

    _itemOnPress = () => {
      navigation.navigate(ExerciseDetailScreen, {
        exercise: this.props.item,
        getChildExercise: this.props.parentFlatList.props.parentDetailView.getChildExercise.bind(
          this.props.parentFlatList.props.parentDetailView
        ),
        exerciseSaving: this.props.parentFlatList.props.parentDetailView.exerciseSaving.bind(
          this.props.parentFlatList.props.parentDetailView
        )
      });
    };

    const swipeSettings = {
      autoClose: true,
      onClose: (secId, rowId, direction) => {
        if (this.state.activeRowKey != null) {
          this.setState({ activeRowKey: null });
        }
      },
      onOpen: (secId, rowId, direction) => {
        this.setState({ activeRowKey: this.props.item.key });
      },
      right: [
        {
          onPress: () => {
            const deletingRow = this.state.activeRowKey;
            Alert.alert(
              "Alert",
              "Are you sure you want to delete ?",
              [
                {
                  text: "No",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel"
                },
                {
                  text: "Yes",
                  onPress: () => {
                    var newExerciseList = this.props.parentFlatList.props.parentDetailView.state.exerciseList;
                    newExerciseList.splice(this.props.index, 1);
                    this.props.parentFlatList.props.parentDetailView.setState({exerciseList: newExerciseList});
                    this.props.parentFlatList.refreshFlatList(deletingRow);
                  }
                }
              ],
              { cancelable: true }
            );
          },
          text: "Delete",
          type: "delete"
        }
      ],
      rowId: this.props.index,
      sectionId: 1
    };

    return (
      <Swipeout {...swipeSettings} style={styles.swipeOut}>
        <TouchableWithoutFeedback onPress={_itemOnPress}>
          <View style={styles.rowContainer}>
            <View style={styles.rowText}>
              <Text
                style={styles.title}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                {this.props.item.name}
              </Text>
              <Text
                style={styles.desc}
                numberOfLines={1}
                ellipsizeMode={"tail"}
              >
                {this.props.item.setList.length.toString() + " set(s)"}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Swipeout>
    );
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: "row",
    backgroundColor: "#FFF",
    height: 80,
    padding: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#CCC",
    shadowOpacity: 1.0,
    shadowRadius: 1
  },
  swipeOut: {
    height: 80,
    marginRight: 10,
    marginLeft: 10,
    marginTop: 1,
    borderRadius: 6
  },
  title: {
    paddingLeft: 10,
    paddingTop: 5,
    fontSize: 16,
    fontWeight: "bold",
    color: "black"
  },
  desc: {
    paddingLeft: 10,
    marginTop: 5,
    fontSize: 14,
    color: "#777"
  },
  rowText: {
    flex: 4,
    flexDirection: "column"
  }
});
