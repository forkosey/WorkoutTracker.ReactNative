import React, {Component} from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  Switch,
  View,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import Button from "react-native-button";
import {AutoGrowingTextInput} from "react-native-autogrow-textinput";
import {Icon} from "react-native-elements";
import SimpleStepper from 'react-native-simple-stepper'

import SetFlatList from "./SetFlatList";
import {WoSetDetailScreen, SaveNewWoSet, SaveWoSet, NewWoSetScreen} from "../../screenNames";

var uuid = require('react-native-uuid');

import {valueTypes} from "../valueTypeEnum";

var set = null;

export default class SetDetail extends Component {
  constructor(props) {
    super(props);
    var key = null;
    var repetition = 0;
    var value = 0;
    var valueType = 0;
    if (set != null) {
      key = set.key;
      repetition = set.repetition;
      value = set.value;
      valueType = set.valueType;
    }
    this.state = {
      set: set,
      key: key,
      setRepetition: repetition,
      setValue: value,
      setValueType: valueType,
      valueTypeStr: valueTypes[valueType][0],
      strRepetition: repetition.toString(),
      strValue: value.toString()
    };
  }

  static navigationOptions = ({navigation}) => {
    const {
      params = {}
    } = navigation.state;
    var headerTitle = "New Set";
    if (navigation.state.routeName == NewWoSetScreen) {
      set = null;
    } else if (navigation.state.routeName == WoSetDetailScreen) {
      set = navigation.getParam("set", null);
      headerTitle = "Set";
    }

    let headerRight = (<Button containerStyle={{
        margin: 5,
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#4997f1"
      }} style={{
        fontSize: 12,
        color: "#4997f1"
      }} onPress={() => {
        params.onSave();
      }}>
      Save
    </Button>);

    let headerBackTitle = "Cancel";

    return {headerTitle, headerRight, headerBackTitle};
  };

  _onSave() {
    if (this.props.navigation.state.params.isSaving == true) {
      return;
    }
    this.props.navigation.setParams({isSaving: true});
    if (this.state.set != null) {
      this.props.navigation.state.params.setSaving({
        key: this.state.key,
        repetition: this.state.setRepetition,
        value: this.state.setValue,
        valueType: this.state.setValueType
      },
      SaveWoSet
    );
    this.props.navigation.setParams({isSaving: false});
    } else {
      this.props.navigation.state.params.setSaving({
        key: uuid.v4(),
        repetition: this.state.setRepetition,
        value: this.state.setValue,
        valueType: this.state.setValueType
      },
      SaveNewWoSet
    );
    this.props.navigation.setParams({isSaving: false});
    }
    this.props.navigation.goBack();
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSave: this._onSave.bind(this),
      isSaving: false
    });
  }

  render() {
    const {navigation} = this.props;

    let mainView = this.props.navigation.state.params && this.props.navigation.state.params.isSaving == true
      ? (<ActivityIndicator style={{
          flex: 1
        }}/>)
      : (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>

          <View style={{height: 120, width: 80, marginBottom: 100}}>
            <Text style={styles.stepperTitle}>Repetition</Text>
            <TextInput
              style={styles.textViews}
              keyboardType='numeric'
              value={this.state.strRepetition}
              onChangeText={text => {
                this.setState({strRepetition: text}, () => {
                  this.setState({setRepetition: Number(text)});
                });
              }}/>
              <SimpleStepper
                tintColor={"#4997f1"}
                initialValue={this.state.setRepetition}
                maximumValue={Number.MAX_VALUE}
                minimumValue={0}
                stepValue={1}
                valueChanged={(value) => {
                  this.setState({setRepetition: value}, () => {
                    this.setState({strRepetition: value.toString()});
                  });
              }}/>
          </View>

          <View style={{height: 120, width: 80}}>
            <Text style={styles.stepperTitle}>{valueTypes[this.state.setValueType][1]}</Text>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
              <TextInput
                style={styles.textViewWithButton}
                keyboardType='numeric'
                value={this.state.strValue}
                onChangeText={text => {
                  this.setState({strValue: text}, () => {
                    this.setState({setValue: Number(text)});
                  });
              }}/>
              <Button
                style={styles.typeButton}
                onPress={() => {
                  if (this.state.setValueType == 5) {
                    this.setState({setValueType: 0}, () => {
                      this.setState({valueTypeStr: valueTypes[this.state.setValueType][0]})
                    });
                  } else {
                    this.setState({setValueType: this.state.setValueType + 1}, () => {
                      this.setState({valueTypeStr: valueTypes[this.state.setValueType][0]})
                    });
                  }
                }}
              >{this.state.valueTypeStr}</Button>
            </View>
            <SimpleStepper
              tintColor={"#4997f1"}
              initialValue={this.state.setValue}
              maximumValue={Number.MAX_VALUE}
              minimumValue={0}
              stepValue={2.5}
              valueChanged={(value) => {
                this.setState({setValue: value}, () => {
                  this.setState({strValue: value.toString()});
                });
            }}/>
          </View>
      </View>
    );

    return mainView;
  }
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: "#f7faff"
  },
  stepperTitle: {
    fontWeight: 'bold',
    fontSize: 14,
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  valueTypeButtonAligment: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textViews: {
    height: 30,
    margin: 15,
    padding: 0,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    fontWeight: "bold",
    textAlign: 'center'
  },
  textViewWithButton: {
    margin: 10,
    padding: 0,
    minWidth: 80,
    maxHeight: 30,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    fontWeight: "bold",
    textAlign: 'center',
    textAlignVertical: 'center',
    flex: 3
  },
  typeButton: {
    flex: 1,
    margin: 5,
    padding: 2,
    maxHeight: 28,
    fontWeight: 'normal',
    borderColor: "#4997f1",
    borderWidth: 1.5,
    borderRadius: 6,
    color: '#4997f1'
  }
});
