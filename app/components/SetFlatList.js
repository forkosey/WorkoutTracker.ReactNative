import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";

import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";

import { NewExerciseScreen } from "../../screenNames";

import SetListCell from "./SetListCell";

export default class SetFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deletedRowKey: null
    };
  }

  refreshFlatList = deletedKey => {
    this.setState(prevState => {
      return {
        deletedRowKey: deletedKey
      };
    });
  };

  render() {
    const { navigation } = this.props.parentDetailView.props;

    return (
      <View style={{ flex: 1, backgroundColor: "#f2f7ff" }}>
        <FlatList
          data={this.props.sets}
          extraData={this.props.parentDetailView.state}
          renderItem={({ item, index }) => {
            return (
              <SetListCell
                item={item}
                index={index}
                parentFlatList={this}
              />
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});
