import React, { Component } from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  Switch,
  View,
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView
} from "react-native";
import Button from "react-native-button";
import { AutoGrowingTextInput } from "react-native-autogrow-textinput";
import { Icon } from "react-native-elements";

import ExerciseFlatList from "./ExerciseFlatList";
import {
  WorkoutDetailScreen,
  NewWorkoutScreen,
  SaveWorkout,
  SaveNewWorkout,
  NewExerciseScreen,
  SaveNewExercise,
  SaveExercise
} from "../../screenNames";

var uuid = require('react-native-uuid');

var workout = null;

export default class WorkoutDetail extends Component {
  constructor(props) {
    super(props);
    var key = null;
    var name = "Workout Name";
    var desc = "Description";
    var exerciseList = [];
    var favorite = false;
    if (workout != null) {
      key = workout.key;
      name = workout.name;
      desc = workout.desc;
      exerciseList = workout.exerciseList;
      favorite = workout.isFavorite;
    }
    this.state = {
      workout: workout,
      key: key,
      workoutName: name,
      workoutDesc: desc,
      exerciseList: exerciseList,
      isFavorite: favorite
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    var headerTitle = "New Workout";
    if (navigation.state.routeName == NewWorkoutScreen) {
      workout = null;
    } else if (navigation.state.routeName == WorkoutDetailScreen) {
      workout = navigation.getParam("workout", null);
      headerTitle = workout.name;
    }

    let headerRight = (
      <Button
        containerStyle={{
          margin: 5,
          padding: 10,
          borderRadius: 10,
          borderWidth: 1,
          borderColor: "#4997f1"
        }}
        style={{ fontSize: 12, color: "#4997f1" }}
        onPress={() => {
          params.onSave();
        }}
      >
        Save
      </Button>
    );

    let headerBackTitle = "cancel";

    return { headerTitle, headerRight, headerBackTitle };
  };

  getChildExercise = (exercise, saveType) => {
    this.setState({
      childScreenExercise: exercise,
      saveType: saveType
    });
  };

  exerciseSaving = (exercise, saveType) => {
    if (saveType == SaveNewExercise) {
      var newExerciseList = this.state.exerciseList;
      newExerciseList.push(exercise);
      this.setState({exerciseList: newExerciseList});
    } else if (saveType == SaveExercise) {
      var newExerciseList = this.state.exerciseList;
      var index = 0;
      for (var i = 0; i < newExerciseList.length; i++) {
        if (newExerciseList[i].key == exercise.key) {
          index = i;
          break;
        }
      }
      newExerciseList[index] = exercise;
      this.setState({exerciseList: newExerciseList});
    }
  };

  _onSave() {
    if (this.props.navigation.state.params.isSaving == true) {
      return;
    }
    this.props.navigation.setParams({ isSaving: true });
    if (this.state.workout != null) {
      this.props.navigation.state.params.workoutSaving(
        {
          key: this.state.key,
          name: this.state.workoutName,
          desc: this.state.workoutDesc,
          exerciseList: this.state.exerciseList,
          isFavorite: this.state.isFavorite
        },
        SaveWorkout
      );
      this.props.navigation.setParams({ isSaving: false });
    } else {
      this.props.navigation.setParams({ isSaving: false });
      this.props.navigation.state.params.workoutSaving(
        {
          key: uuid.v4(),
          name: this.state.workoutName,
          desc: this.state.workoutDesc,
          exerciseList: this.state.exerciseList,
          isFavorite: this.state.isFavorite
        },
        SaveNewWorkout
      );
      this.props.navigation.setParams({ isSaving: false });
    }
    this.props.navigation.goBack();
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSave: this._onSave.bind(this),
      isSaving: false
    });
  }

  render() {
    const { navigation } = this.props;

    _newExerciseButtonOnPress = () => {
      navigation.navigate(NewExerciseScreen, {
        getChildExercise: this.getChildExercise.bind(this),
        exerciseSaving: this.exerciseSaving.bind(this)
      });
    };

    let mainView =
      this.props.navigation.state.params &&
      this.props.navigation.state.params.isSaving == true ? (
        <ActivityIndicator style={{ flex: 1 }} />
      ) : (
        <View style={styles.mainView}>
          <TextInput
            style={styles.workoutName}
            onSubmitEditing={Keyboard.dismiss}
            underlineColorAndroid="transparent"
            placeholder={this.state.workoutName}
            onChangeText={text => {
              this.setState({ workoutName: text });
            }}
            value={this.state.workoutName}
          />
          <TextInput
            style={styles.workoutDesc}
            multiline={true}
            underlineColorAndroid="transparent"
            placeholder={this.state.workoutDesc}
            onChangeText={text => {
              this.setState({ workoutDesc: text });
            }}
            value={this.state.workoutDesc}
          />
          <View style={styles.exerciseListHeader}>
            <Icon name="add" color="rgba(0,0,0,0)" />
            <Text style={{ fontWeight: "bold", fontSize: 16 }}>Exercises</Text>
            <Icon name="add" color="#4997f1" onPress={_newExerciseButtonOnPress}/>
          </View>
          <ExerciseFlatList
            exercises={this.state.exerciseList}
            parentDetailView={this}
          />
          <Text style={styles.favoriteSwitchLabel}>Mark as favorite</Text>
          <Switch
            style={styles.favoriteSwitch}
            value={this.state.isFavorite}
            onValueChange={() => {
              this.setState({ isFavorite: !this.state.isFavorite });
            }}
          />
        </View>
      );

    return mainView;
  }
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#f7faff"
  },
  workoutName: {
    marginHorizontal: 10,
    marginTop: 8,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    fontWeight: "bold"
  },
  workoutDesc: {
    height: 150,
    marginHorizontal: 10,
    marginTop: 8,
    borderColor: "#cccccc",
    borderWidth: 1.5,
    borderRadius: 6,
    textAlignVertical: "top"
  },
  exerciseListHeader: {
    backgroundColor: "#e3e9f2",
    marginTop: 8,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 8
  },
  favoriteSwitchLabel: {
    marginTop: 8,
    height: 25,
    alignSelf: "center"
  },
  favoriteSwitch: {
    marginBottom: 8,
    height: 25,
    alignSelf: "center"
  }
});
