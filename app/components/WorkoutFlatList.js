import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View, TextInput } from "react-native";

import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";

import {
  NewWorkoutScreen,
  SaveWorkout,
  SaveNewWorkout
} from "../../screenNames";

import WorkoutListCell from "./WorkoutListCell";
import workouts from "../data/WorkoutData";

export default class WorkoutFlatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deletedRowKey: null,
      refreshing: false,
      childScreenWorkout: null,
      saveType: null,
      workouts: workouts
    };
  }

  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Workouts";
    return { headerTitle };
  };

  getChildWorkout = (workout, saveType) => {
    this.setState({
      childScreenWorkout: workout,
      saveType: saveType
    });
  };

  workoutSaving = (workout, saveType) => {
    if (saveType == SaveNewWorkout) {
      var newWorkouts = this.state.workouts;
      newWorkouts.push(workout);
      this.setState({ workouts: newWorkouts });
    } else if (saveType == SaveWorkout) {
      var newWorkouts = this.state.workouts;
      var index = 0;
      for (var i = 0; i < newWorkouts.length; i++) {
        if (newWorkouts[i].key == workout.key) {
          index = i;
          break;
        }
      }
      newWorkouts[index] = workout
      this.setState({ workouts: newWorkouts });
    }
  };

  refreshFlatList = deletedKey => {
    this.setState(prevState => {
      return {
        deletedRowKey: deletedKey
      };
    });
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      },
      () => {
        this.setState({ refreshing: false });
      }
    );
  };

  render() {
    const { navigation } = this.props;

    _newWorkoutButtonOnPress = () => {
      navigation.navigate(NewWorkoutScreen, {
        getChildWorkout: this.getChildWorkout.bind(this),
        workoutSaving: this.workoutSaving.bind(this)
      });
    };

    var newWorkoutButtonSettings = {
      buttonColor: "#bababa",
      title: "New Workout",
      size: 46,
      onPress: _newWorkoutButtonOnPress
    };

    return (
      <View style={{ flex: 1, backgroundColor: "#f2f7ff" }}>
        <FlatList
          data={this.state.workouts}
          extraData={this.state}
          renderItem={({ item, index }) => {
            return (
              <WorkoutListCell
                item={item}
                index={index}
                parentFlatList={this}
              />
            );
          }}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
        />
        <ActionButton buttonColor="#4997f1">
          <ActionButton.Item {...newWorkoutButtonSettings}>
            <Icon name="md-walk" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white"
  }
});
