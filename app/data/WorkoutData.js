var uuid = require('react-native-uuid');

var workouts = [
  {
    key: uuid.v4(),
    name: "Chest1",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: true
  },
  {
    key: uuid.v4(),
    name: "Leg1",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: false
  },
  {
    key: uuid.v4(),
    name: "Back1",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: true
  },
  {
    key: uuid.v4(),
    name: "Shoulder1",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: false
  },
  {
    key: uuid.v4(),
    name: "Chest2",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: true
  },
  {
    key: uuid.v4(),
    name: "Leg2",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: false
  },
  {
    key: uuid.v4(),
    name: "Back2",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: false
  },
  {
    key: uuid.v4(),
    name: "Shoulder2",
    desc: "x exercise(s)",
    exerciseList: [
      {
        key: uuid.v4(),
        name: "Bench Press",
        desc: "Flat bench press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 200,
            valueType: 0
          }
        ]
      },
      {
        key: uuid.v4(),
        name: "Incline Dumbell Press",
        desc: "Incline dumbell press.",
        setList: [
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          },
          {
            key: uuid.v4(),
            repetition: 10,
            value: 80,
            valueType: 0
          }
        ]
      }
    ],
    isFavorite: true
  }
];
module.exports = workouts;
