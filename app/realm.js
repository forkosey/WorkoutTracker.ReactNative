import Realm from 'realm';

class WoSet extends Realm.Object {}
WoSet.schema = {
  name: 'WoSet',
  properties: {
    repetition: 'int',
    value: 'float',
    valueType: 'string'
  }
};

class Exercise extends Realm.Object {}
Exercise.schema = {
  name: 'Exercise',
  properties: {
    name: 'string',
    description: 'string',
    setList: 'WoSet[]'
  }
};

class Workout extends Realm.Object {}
Workout.schema = {
  name: 'Workout',
  properties: {
    name: 'string',
    description: 'string',
    exerciseList: 'Exercise[]'
  },
  isFavorite: 'Boolean'
};

Realm.open({Schema: [WoSet.schema, Exercise.schema, Workout.schema]}).then(realm => {
  if (realm.objects('Workout').length == 0) {
    realm.write(() => {
      const newWo = realm.create('Workout', {
        name: 'wo1',
        description: 'desc1',
        exerciseList: [
          {
            name: 'ex1',
            description: 'desc1',
            setList: [
              {
                repetition: 12,
                value: 100,
                valueType: 'lbs'
              },
              {
                repetition: 12,
                value: 100,
                valueType: 'lbs'
              }
            ]
          },
          {
            name: 'ex2',
            description: 'desc2',
            setList: [
              {
                repetition: 12,
                value: 100,
                valueType: 'lbs'
              },
              {
                repetition: 12,
                value: 100,
                valueType: 'lbs'
              }
            ]
          }
        ],
        isFavorite: true
      });
    });
  }
});

export default new Realm({schema: [WoSet, Exercise, Workout]});
