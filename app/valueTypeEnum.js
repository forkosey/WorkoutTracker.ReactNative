const valueTypes = [
  ['lbs', 'Weight'],
  ['kg', 'Weight'],
  ['m', 'Distance'],
  ['km', 'Distance'],
  ['sec', 'Time'],
  ['min', 'Time']
]

export {valueTypes};
