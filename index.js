import { YellowBox } from "react-native";
import { AppRegistry } from "react-native";
import { createStackNavigator } from "react-navigation";

import {
  WorkoutListScreen,
  WorkoutDetailScreen, NewWorkoutScreen, SaveWorkout, SaveNewWorkout,
  ExerciseDetailScreen, NewExerciseScreen, SaveExercise, SaveNewExercise,
  WoSetDetailScreen, NewWoSetScreen, SaveWoSet, SaveNewWoSet
} from "./screenNames";

import WorkoutFlatList from "./app/components/WorkoutFlatList";

import WorkoutDetail from "./app/components/WorkoutDetail";

import ExerciseDetail from "./app/components/ExerciseDetail";

import SetDetail from "./app/components/SetDetail";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);

const App = createStackNavigator({
  WorkoutListScreen: {
    screen: WorkoutFlatList
  },
  WorkoutDetailScreen: {
    screen: WorkoutDetail
  },
  NewWorkoutScreen: {
    screen: WorkoutDetail
  },
  SaveWorkout: {
    screen: WorkoutFlatList
  },
  SaveNewWorkout: {
    screen: WorkoutFlatList
  },
  ExerciseDetailScreen: {
    screen: ExerciseDetail
  },
  NewExerciseScreen: {
    screen: ExerciseDetail
  },
  SaveExercise: {
    screen: WorkoutDetail
  },
  SaveNewExercise: {
    screen: WorkoutDetail
  },
  WoSetDetailScreen: {
    screen: SetDetail
  },
  NewWoSetScreen: {
    screen: SetDetail
  },
  SaveWoSet: {
    screen: ExerciseDetail
  },
  SaveNewWoSet: {
    screen: ExerciseDetail
  }
});

AppRegistry.registerComponent("WorkoutTracker", () => App);
