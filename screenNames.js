const WorkoutListScreen = "WorkoutListScreen";

const NewWorkoutScreen = "NewWorkoutScreen";
const WorkoutDetailScreen = "WorkoutDetailScreen";
const SaveWorkout = "SaveWorkout";
const SaveNewWorkout = "SaveNewWorkout";

const NewExerciseScreen = "NewExerciseScreen";
const ExerciseDetailScreen = "ExerciseDetailScreen";
const SaveExercise = "SaveExercise";
const SaveNewExercise = "SaveNewExercise";

const NewWoSetScreen = "NewWoSetScreen";
const WoSetDetailScreen = "WoSetDetailScreen";
const SaveWoSet = "SaveWoSet";
const SaveNewWoSet = "SaveNewWoSet";

export {
  WorkoutListScreen,
  WorkoutDetailScreen, NewWorkoutScreen, SaveWorkout, SaveNewWorkout,
  ExerciseDetailScreen, NewExerciseScreen, SaveExercise, SaveNewExercise,
  WoSetDetailScreen, NewWoSetScreen, SaveWoSet, SaveNewWoSet
 };
